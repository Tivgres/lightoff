package co.tivgres.lightoff.helperGame;

import java.util.Arrays;
import java.util.Random;

/**
 * Created by tivgres on 12/30/16.
 */

public class Game {
    public static int[][] mass;
    int[][] mass_empty;
    public int[][] Create(int size) {
        Random r = new Random();
        int[][] mass = new int[size][size];
        for (int k = 0; k < size; k++) {
            // цикл по первой размерности
            for (int i = 0; i < size; i++) {
                // цикл по второй размерности
                for (int j = 0; j < size; j++) {
                    int c = r.nextInt(size);
                    //инициализация элементов массива
                    if ((c & 1) == 0) {
                        c = 0;
                        // четное
                        mass[i][j] = c;
                    } else {
                        c = 1;
                        // нечетное
                        mass[i][j] = c;
                    }
                }
            }
        }
        return mass;
    }

    public void Clear(int size) {
        mass = Create(size);
        //reset field
        Reset_field.reset_field();
    }


    public int[][] Get(int size, boolean reset, boolean test_mode) {
        int[][] mass_zero = new int[size][size];
        if (Arrays.deepEquals(mass, mass_empty) || reset && !test_mode) {
            mass = Create(size);
            //reset field
            Reset_field.reset_field();
        } else if (mass.length < size || mass.length > size || reset && !test_mode) {
            mass = Create(size);
            //reset field
            Reset_field.reset_field();
        } else if (!Arrays.deepEquals(mass, mass_empty) && mass.length == size && !reset && !test_mode) {
            if (Arrays.deepEquals(mass, mass_zero)) {
                mass = Create(size);
                //reset field
                Reset_field.reset_field();
            } else {
                return mass;
            }
        } else if (test_mode) {
            int[][] mass_test = new int[size][size];
            mass_test[0][0] = 1;
            mass_test[0][1] = 1;
            mass_test[1][0] = 1;
            Reset_field.reset_field();
            return mass_test;
        }
        return mass;
    }

}
