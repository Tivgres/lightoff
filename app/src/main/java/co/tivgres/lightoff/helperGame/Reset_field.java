package co.tivgres.lightoff.helperGame;

import static co.tivgres.lightoff.GameActivity.count;
import static co.tivgres.lightoff.GameActivity.diff_time;
import static co.tivgres.lightoff.GameActivity.pause_time;
import static co.tivgres.lightoff.GameActivity.reset;
import static co.tivgres.lightoff.GameActivity.round_time;
import static co.tivgres.lightoff.GameActivity.start_time;
import static co.tivgres.lightoff.GameActivity.test_mode;

/**
 * Created by tivgres on 1/8/17.
 */

public class Reset_field {

    public static void reset_field() {
        start_time = 0;
        pause_time = 0;
        //resume_time = 0;
        diff_time = 0;
        round_time = 0;
        count = 0;
        reset = false;
        test_mode = false;
    }
}
