package co.tivgres.lightoff;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.Arrays;

/**
 * Created by tivgres on 1/19/17.
 */

public class IntroLevelActivity extends Activity {
    TableLayout tl_game_intro;
    TextView tv_level;
    TextView tv_size;
    Button btn[][];
    Button button;
    int level_of_intro = 1;
    int size_of_intro = 3;
    //for listener
    //v3
    //l1
    static boolean count_l1_v3_11;
    //l2
    static boolean count_l2_v3_00;
    static boolean count_l2_v3_22;
    //l3
    static boolean count_l3_v3_02;
    static boolean count_l3_v3_22;
    static boolean count_l3_v3_11;
    //v5
    //l1
    static boolean count_l1_v5_22;
    //l2
    static boolean count_l2_v5_00;
    static boolean count_l2_v5_04;
    static boolean count_l2_v5_40;
    static boolean count_l2_v5_44;
    static boolean count_l2_v5_22;
    //l3
    static boolean count_l3_v5_11;
    static boolean count_l3_v5_12;
    static boolean count_l3_v5_13;
    static boolean count_l3_v5_00;
    static boolean count_l3_v5_04;
    static boolean count_l3_v5_40;
    static boolean count_l3_v5_44;
    static boolean count_l3_v5_22;
    //v6
    //l1
    static boolean count_l1_v6_33;
    //l2
    static boolean count_l2_v6_00;
    static boolean count_l2_v6_05;
    static boolean count_l2_v6_55;
    static boolean count_l2_v6_50;
    static boolean count_l2_v6_33;
    //l3
    static boolean count_l3_v6_23;
    static boolean count_l3_v6_22;
    static boolean count_l3_v6_24;
    static boolean count_l3_v6_00;
    static boolean count_l3_v6_05;
    static boolean count_l3_v6_50;
    static boolean count_l3_v6_55;
    static boolean count_l3_v6_33;
    static boolean end;
    AlertDialog.Builder ad;
    Context context;

    int[][] mass_empty;
    int[][] mass;

    void reset_counts() {
        count_l1_v3_11 = false;
        count_l2_v3_00 = false;
        count_l2_v3_22 = false;
        count_l3_v3_02 = false;
        count_l3_v3_22 = false;
        count_l3_v3_11 = false;
        count_l1_v5_22 = false;
        count_l2_v5_00 = false;
        count_l2_v5_04 = false;
        count_l2_v5_40 = false;
        count_l2_v5_44 = false;
        count_l2_v5_22 = false;
        count_l3_v5_11 = false;
        count_l3_v5_12 = false;
        count_l3_v5_13 = false;
        count_l3_v5_00 = false;
        count_l3_v5_04 = false;
        count_l3_v5_40 = false;
        count_l3_v5_44 = false;
        count_l3_v5_22 = false;
        count_l1_v6_33 = false;
        count_l2_v6_00 = false;
        count_l2_v6_05 = false;
        count_l2_v6_55 = false;
        count_l2_v6_50 = false;
        count_l2_v6_33 = false;
        count_l3_v6_23 = false;
        count_l3_v6_22 = false;
        count_l3_v6_24 = false;
        count_l3_v6_00 = false;
        count_l3_v6_05 = false;
        count_l3_v6_50 = false;
        count_l3_v6_55 = false;
        count_l3_v6_33 = false;
        end = false;
    }

    void checker() {
        if (Arrays.deepEquals(mass, mass_empty)) {
            end = true;
            alert_dialog();
            ad.show();
            reset_counts();
        }
    }

    void alert_dialog() {
        ad.setTitle(getResources().getString(R.string.attention));
        if (end) {
            ad.setMessage(getResources().getString(R.string.finish_tutor));
        } else if (!end) {
            ad.setMessage(getResources().getString(R.string.alert_tutor));
        }
        ad.setPositiveButton(R.string.close, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {

            }
        });
    }


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro_level);
        //Full screen mode
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        tl_game_intro = (TableLayout) findViewById(R.id.tl_intro_game);
        tv_level = (TextView) findViewById(R.id.tv_intro_level);
        tv_size = (TextView) findViewById(R.id.tv_intro_size);
        tv_level.setText(getResources().getString(R.string.level) + " " + level_of_intro);
        tv_size.setText(getResources().getString(R.string.size) + " " + size_of_intro + "x" + size_of_intro);
        build_intro_lvl();
    }

    protected void onResume() {
        super.onResume();
        context = IntroLevelActivity.this;
        ad = new AlertDialog.Builder(context);
        end = false;
        alert_dialog();
        ad.show();
    }

    public int[][] calc_mass() {
        //mass_intro = new int[size_of_intro][size_of_intro];
        int x = size_of_intro;
        int y = size_of_intro;
        int center = (int) Math.ceil(x / 2);
        int[][] massive = new int[x][y];
        if (level_of_intro == 1) {
            //just center
            massive[center][center] = 1;
            massive[center][center + 1] = 1;
            massive[center][center - 1] = 1;
            massive[center + 1][center] = 1;
            massive[center - 1][center] = 1;
        } else if (level_of_intro == 2) {
            if (size_of_intro == 3) {
                //left | up
                massive[0][0] = 1;
                massive[0][1] = 1;
                massive[1][0] = 1;
                //right | bottom
                massive[x - 1][x - 1] = 1;
                massive[x - 2][x - 1] = 1;
                massive[x - 1][x - 2] = 1;
            } else if (size_of_intro == 5 || size_of_intro == 6) {
                //left | up
                massive[0][0] = 1;
                massive[0][1] = 1;
                massive[1][0] = 1;
                //left | bottom
                massive[x - 1][0] = 1;
                massive[x - 1][1] = 1;
                massive[x - 2][0] = 1;
                //center
                massive[center][center + 1] = 1;
                massive[center][center - 1] = 1;
                massive[center][center] = 1;
                massive[center - 1][center] = 1;
                massive[center + 1][center] = 1;
                //right | top
                massive[0][x - 1] = 1;
                massive[0][x - 2] = 1;
                massive[1][x - 1] = 1;
                //right | bottom
                massive[x - 1][x - 1] = 1;
                massive[x - 2][x - 1] = 1;
                massive[x - 1][x - 2] = 1;
            }

        } else if (level_of_intro == 3) {
            if (size_of_intro == 3) {
                massive[0][2] = 1;
                massive[1][0] = 1;
                massive[1][1] = 1;
                massive[1][2] = 1;
                massive[2][2] = 1;
            } else if (size_of_intro == 5) {
                massive[0][0] = 1;
                massive[0][2] = 1;
                massive[0][4] = 1;
                massive[3][0] = 1;
                massive[3][2] = 1;
                massive[3][4] = 1;
                massive[4][0] = 1;
                massive[4][1] = 1;
                massive[4][3] = 1;
                massive[4][4] = 1;
            } else if (size_of_intro == 6) {
                massive[0][0] = 1;
                massive[0][1] = 1;
                massive[0][4] = 1;
                massive[0][5] = 1;
                massive[1][0] = 1;
                massive[1][2] = 1;
                massive[1][3] = 1;
                massive[1][4] = 1;
                massive[1][5] = 1;
                massive[2][1] = 1;
                massive[2][5] = 1;
                massive[4][0] = 1;
                massive[4][3] = 1;
                massive[4][5] = 1;
                massive[5][0] = 1;
                massive[5][1] = 1;
                massive[5][4] = 1;
                massive[5][5] = 1;
            }
        }
        return massive;
    }

    //get size lcd
    public Point Size() {
        Point size_lcd = new Point();
        Display display = getWindowManager().getDefaultDisplay();
        display.getSize(size_lcd);
        return size_lcd;
    }

    //set background dynamic for lamp on
    public void setBackgroundIntro(int size, int i, int j) {
        if (size == 3) {
            btn[i][j].setBackgroundResource(R.mipmap.lamp_on_v3_intro);
        } else if (size == 5) {
            btn[i][j].setBackgroundResource(R.mipmap.lamp_on_v5_intro);
        } else if (size == 6) {
            btn[i][j].setBackgroundResource(R.mipmap.lamp_on_v6_intro);
        }
    }

    //set background dynamic for lamp on
    public void setBackgroundOn(int size, int i, int j) {
        if (size == 3) {
            btn[i][j].setBackgroundResource(R.mipmap.lamp_on_v3);
        } else if (size == 5) {
            btn[i][j].setBackgroundResource(R.mipmap.lamp_on_v5);
        } else if (size == 6) {
            btn[i][j].setBackgroundResource(R.mipmap.lamp_on_v6);
        }
    }

    //set background dynamic for lamp off
    public void setBackgroundOff(int size, int i, int j) {
        if (size == 3) {
            btn[i][j].setBackgroundResource(R.mipmap.lamp_off_v3);
        } else if (size == 5) {
            btn[i][j].setBackgroundResource(R.mipmap.lamp_off_v5);
        } else if (size == 6) {
            btn[i][j].setBackgroundResource(R.mipmap.lamp_off_v6);
        }
    }


    public void build_intro_lvl() {
        btn = new Button[size_of_intro][size_of_intro];
        mass_empty = new int[size_of_intro][size_of_intro];
        mass = calc_mass();
        //set sizes
        int width_lcd = Size().x;
        int height_lcd = Size().y;
        //check orient
        if (width_lcd > height_lcd) {
            width_lcd = (int) Math.floor(height_lcd / 1.5);
        }
        //calculate sizes
        int width_btn = width_lcd / size_of_intro;
        int height_btn = width_btn;
        int width_row = width_lcd;
        int height_row = height_btn;
        /*//create button
        Button[][] btn = new Button[size][size];*/
        for (int i = 0, lenI = mass.length; i < lenI; i++) {
            TableRow row = new TableRow(this); // create row
            for (int j = 0, lenJ = mass[i].length; j < lenJ; j++) {
                button = new Button(this);
                btn[i][j] = button;
                button.setMaxWidth(width_btn);
                button.setWidth(width_btn);
                button.setMinimumWidth(width_btn);
                button.setMaxHeight(height_btn);
                button.setHeight(height_btn);
                button.setMinHeight(height_btn);
                button.setOnClickListener(new IntroLevelActivity.Listener(i, j, size_of_intro, mass)); // add listener
                row.addView(button); // create button
                //set background button
                if (mass[i][j] == 1) {
                    setBackgroundOn(size_of_intro, i, j);
                }
                if (mass[i][j] == 0) {
                    setBackgroundOff(size_of_intro, i, j);
                }
            }
            tl_game_intro.addView(row, new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,
                    TableLayout.LayoutParams.WRAP_CONTENT)); // добавление строки в таблицу
        }
        if (level_of_intro == 1 && size_of_intro == 3) {
            setBackgroundIntro(size_of_intro, 1, 1);
        } else if (level_of_intro == 1 && size_of_intro == 5) {
            setBackgroundIntro(size_of_intro, 2, 2);
        } else if (level_of_intro == 1 && size_of_intro == 6) {
            setBackgroundIntro(size_of_intro, 3, 3);
        } else if (level_of_intro == 2 && size_of_intro == 3) {
            setBackgroundIntro(size_of_intro, 0, 0);
        } else if (level_of_intro == 2 && size_of_intro == 5) {
            setBackgroundIntro(size_of_intro, 0, 0);
        } else if (level_of_intro == 2 && size_of_intro == 6) {
            setBackgroundIntro(size_of_intro, 0, 0);
        }
        //3lvl
        else if (level_of_intro == 3 && size_of_intro == 3) {
            setBackgroundIntro(size_of_intro, 0, 2);
        } else if (level_of_intro == 3 && size_of_intro == 5) {
            setBackgroundIntro(size_of_intro, 1, 1);
        } else if (level_of_intro == 3 && size_of_intro == 6) {
            setBackgroundIntro(size_of_intro, 2, 2);
        }
    }

    public void level_calc(boolean left, boolean right) {
        if (level_of_intro == 1) {
            if (left && !right) {
                level_of_intro = 3;
            } else if (right && !left) {
                level_of_intro = 2;
            }
        } else if (level_of_intro == 2) {
            if (left && !right) {
                level_of_intro = 1;
            } else if (right && !left) {
                level_of_intro = 3;
            }
        } else if (level_of_intro == 3) {
            if (left && !right) {
                level_of_intro = 2;
            } else if (right && !left) {
                level_of_intro = 1;
            }
        }
        tv_level.setText(getResources().getString(R.string.level) + " " + level_of_intro);
    }

    public void size_calc(boolean left, boolean right) {
        if (size_of_intro == 3) {
            if (left && !right) {
                size_of_intro = 6;
            } else if (right && !left) {
                size_of_intro = 5;
            }
        } else if (size_of_intro == 5) {
            if (left && !right) {
                size_of_intro = 3;
            } else if (right && !left) {
                size_of_intro = 6;
            }
        } else if (size_of_intro == 6) {
            if (left && !right) {
                size_of_intro = 5;
            } else if (right && !left) {
                size_of_intro = 3;
            }
        }
        tv_size.setText(getResources().getString(R.string.size) + " " + size_of_intro + "x" + size_of_intro);
    }


    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_intro_left_level:
                tl_game_intro.removeAllViews();
                level_calc(true, false);
                alert_dialog();
                build_intro_lvl();
                reset_counts();
                break;
            case R.id.btn_intro_right_level:
                tl_game_intro.removeAllViews();
                level_calc(false, true);
                alert_dialog();
                build_intro_lvl();
                reset_counts();
                break;
            case R.id.btn_intro_left_size:
                tl_game_intro.removeAllViews();
                size_calc(true, false);
                alert_dialog();
                build_intro_lvl();
                reset_counts();
                break;
            case R.id.btn_intro_right_size:
                tl_game_intro.removeAllViews();
                size_calc(false, true);
                alert_dialog();
                build_intro_lvl();
                reset_counts();
                break;
        }
    }


    //onclick custom
    public class Listener implements View.OnClickListener {
        int x = 0;
        int y = 0;
        int level = 0;
        int[][] massive;
        public TableLayout layout;

        Listener(int i, int j, int size, int[][] mass) {
            this.x = i;
            this.y = j;
            this.level = size;
            this.massive = mass;
        }


        @Override
        public void onClick(View v) {
            if (level_of_intro == 1) {
                if (size_of_intro == 3) {
                    if (x == 1 && y == 1) {
                        change_all();
                        count_l1_v3_11 = true;
                    } else {
                        end = false;
                        ad.show();
                    }
                } else if (size_of_intro == 5) {
                    if (x == 2 && y == 2) {
                        change_all();
                        count_l1_v5_22 = true;
                    } else {
                        end = false;
                        ad.show();
                    }
                } else if (size_of_intro == 6) {
                    if (x == 3 && y == 3) {
                        change_all();
                        count_l1_v6_33 = true;
                    } else {
                        end = false;
                        ad.show();
                    }
                }
            } else if (level_of_intro == 2) {
                if (size_of_intro == 3) {
                    if (!count_l2_v3_00) {
                        if (x == 0 && y == 0) {
                            change_all();
                            count_l2_v3_00 = true;
                            setBackgroundIntro(size_of_intro, 2, 2);
                        } else {
                            end = false;
                            ad.show();
                        }
                    } else if (count_l2_v3_00 && !count_l2_v3_22) {
                        if (x == 2 && y == 2) {
                            change_all();
                            count_l2_v3_22 = true;
                        } else {
                            end = false;
                            ad.show();
                        }
                    }
                } else if (size_of_intro == 5) {
                    if (!count_l2_v5_00) {
                        if (x == 0 && y == 0) {
                            change_all();
                            count_l2_v5_00 = true;
                            setBackgroundIntro(size_of_intro, 0, 4);
                        } else {
                            end = false;
                            ad.show();
                        }
                    } else if (count_l2_v5_00 && !count_l2_v5_04) {
                        if (x == 0 && y == 4) {
                            change_all();
                            count_l2_v5_04 = true;
                            setBackgroundIntro(size_of_intro, 4, 4);
                        } else {
                            end = false;
                            ad.show();
                        }
                    } else if (count_l2_v5_04 && !count_l2_v5_44) {
                        if (x == 4 && y == 4) {
                            change_all();
                            count_l2_v5_44 = true;
                            setBackgroundIntro(size_of_intro, 4, 0);
                        } else {
                            end = false;
                            ad.show();
                        }
                    } else if (count_l2_v5_44 && !count_l2_v5_40) {
                        if (x == 4 && y == 0) {
                            change_all();
                            count_l2_v5_40 = true;
                            setBackgroundIntro(size_of_intro, 2, 2);
                        } else {
                            end = false;
                            ad.show();
                        }
                    } else if (count_l2_v5_40 && !count_l2_v5_22) {
                        if (x == 2 && y == 2) {
                            change_all();
                            count_l2_v5_22 = true;
                        } else {
                            end = false;
                            ad.show();
                        }
                    }
                } else if (size_of_intro == 6) {
                    if (!count_l2_v6_00) {
                        if (x == 0 && y == 0) {
                            change_all();
                            count_l2_v6_00 = true;
                            setBackgroundIntro(size_of_intro, 0, 5);
                        } else {
                            end = false;
                            ad.show();
                        }
                    } else if (count_l2_v6_00 && !count_l2_v6_05) {
                        if (x == 0 && y == 5) {
                            change_all();
                            count_l2_v6_05 = true;
                            setBackgroundIntro(size_of_intro, 5, 5);
                        } else {
                            end = false;
                            ad.show();
                        }
                    } else if (count_l2_v6_05 && !count_l2_v6_55) {
                        if (x == 5 && y == 5) {
                            change_all();
                            count_l2_v6_55 = true;
                            setBackgroundIntro(size_of_intro, 5, 0);
                        } else {
                            end = false;
                            ad.show();
                        }
                    } else if (count_l2_v6_55 && !count_l2_v6_50) {
                        if (x == 5 && y == 0) {
                            change_all();
                            count_l2_v6_50 = true;
                            setBackgroundIntro(size_of_intro, 3, 3);
                        } else {
                            end = false;
                            ad.show();
                        }
                    } else if (count_l2_v6_50 && !count_l2_v6_33) {
                        if (x == 3 && y == 3) {
                            change_all();
                            count_l2_v6_33 = true;
                        } else {
                            end = false;
                            ad.show();
                        }
                    }
                }
            } else if (level_of_intro == 3) {
                if (size_of_intro == 3) {
                    if (!count_l3_v3_02) {
                        if (x == 0 && y == 2) {
                            change_all();
                            count_l3_v3_02 = true;
                            setBackgroundIntro(size_of_intro, 2, 2);
                        } else {
                            end = false;
                            ad.show();
                        }
                    } else if (count_l3_v3_02 && !count_l3_v3_22) {
                        if (x == 2 && y == 2) {
                            change_all();
                            count_l3_v3_22 = true;
                            setBackgroundIntro(size_of_intro, 1, 1);
                        } else {
                            end = false;
                            ad.show();
                        }
                    } else if (count_l3_v3_22 && !count_l3_v3_11) {
                        if (x == 1 && y == 1) {
                            change_all();
                            count_l3_v3_11 = true;
                        } else {
                            end = false;
                            ad.show();
                        }
                    }
                } else if (size_of_intro == 5) {
                    if (!count_l3_v5_11) {
                        if (x == 1 && y == 1) {
                            change_all();
                            count_l3_v5_11 = true;
                            setBackgroundIntro(size_of_intro, 1, 2);
                        } else {
                            end = false;
                            ad.show();
                        }
                    } else if (count_l3_v5_11 && !count_l3_v5_12) {
                        if (x == 1 && y == 2) {
                            change_all();
                            count_l3_v5_12 = true;
                            setBackgroundIntro(size_of_intro, 1, 3);
                        } else {
                            end = false;
                            ad.show();
                        }
                    } else if (count_l3_v5_12 && !count_l3_v5_13) {
                        if (x == 1 && y == 3) {
                            change_all();
                            count_l3_v5_13 = true;
                            setBackgroundIntro(size_of_intro, 0, 0);
                        } else {
                            end = false;
                            ad.show();
                        }
                    } else if (count_l3_v5_13 && !count_l3_v5_00) {
                        if (x == 0 && y == 0) {
                            change_all();
                            count_l3_v5_00 = true;
                            setBackgroundIntro(size_of_intro, 0, 4);
                        } else {
                            end = false;
                            ad.show();
                        }
                    } else if (count_l3_v5_00 && !count_l3_v5_04) {
                        if (x == 0 && y == 4) {
                            change_all();
                            count_l3_v5_04 = true;
                            setBackgroundIntro(size_of_intro, 4, 0);
                        } else {
                            end = false;
                            ad.show();
                        }
                    } else if (count_l3_v5_04 && !count_l3_v5_40) {
                        if (x == 4 && y == 0) {
                            change_all();
                            count_l3_v5_40 = true;
                            setBackgroundIntro(size_of_intro, 4, 4);
                        } else {
                            end = false;
                            ad.show();
                        }
                    } else if (count_l3_v5_40 && !count_l3_v5_44) {
                        if (x == 4 && y == 4) {
                            change_all();
                            count_l3_v5_44 = true;
                            setBackgroundIntro(size_of_intro, 2, 2);
                        } else {
                            end = false;
                            ad.show();
                        }
                    } else if (count_l3_v5_44 && !count_l3_v5_22) {
                        if (x == 2 && y == 2) {
                            change_all();
                            count_l3_v5_22 = true;
                        } else {
                            end = false;
                            ad.show();
                        }
                    }
                } else if (size_of_intro == 6) {
                    if (!count_l3_v6_22) {
                        if (x == 2 && y == 2) {
                            change_all();
                            count_l3_v6_22 = true;
                            setBackgroundIntro(size_of_intro, 2, 3);
                        } else {
                            end = false;
                            ad.show();
                        }
                    } else if (count_l3_v6_22 && !count_l3_v6_23) {
                        if (x == 2 && y == 3) {
                            change_all();
                            count_l3_v6_23 = true;
                            setBackgroundIntro(size_of_intro, 2, 4);
                        } else {
                            end = false;
                            ad.show();
                        }
                    } else if (count_l3_v6_23 && !count_l3_v6_24) {
                        if (x == 2 && y == 4) {
                            change_all();
                            count_l3_v6_24 = true;
                            setBackgroundIntro(size_of_intro, 0, 0);
                        } else {
                            end = false;
                            ad.show();
                        }
                    } else if (count_l3_v6_24 && !count_l3_v6_00) {
                        if (x == 0 && y == 0) {
                            change_all();
                            count_l3_v6_00 = true;
                            setBackgroundIntro(size_of_intro, 0, 5);
                        } else {
                            end = false;
                            ad.show();
                        }
                    } else if (count_l3_v6_00 && !count_l3_v6_05) {
                        if (x == 0 && y == 5) {
                            change_all();
                            count_l3_v6_05 = true;
                            setBackgroundIntro(size_of_intro, 5, 0);
                        } else {
                            end = false;
                            ad.show();
                        }
                    } else if (count_l3_v6_05 && !count_l3_v6_50) {
                        if (x == 5 && y == 0) {
                            change_all();
                            count_l3_v6_50 = true;
                            setBackgroundIntro(size_of_intro, 5, 5);
                        } else {
                            end = false;
                            ad.show();
                        }
                    } else if (count_l3_v6_50 && !count_l3_v6_55) {
                        if (x == 5 && y == 5) {
                            change_all();
                            count_l3_v6_55 = true;
                            setBackgroundIntro(size_of_intro, 3, 3);
                        } else {
                            end = false;
                            ad.show();
                        }
                    } else if (count_l3_v6_55 && !count_l3_v6_33) {
                        if (x == 3 && y == 3) {
                            change_all();
                            count_l3_v6_33 = true;
                        } else {
                            end = false;
                            ad.show();
                        }
                    }
                }
            }
            checker();
        }


        void change_all() {
            xy();
            left_x();
            right_x();
            bottom_y();
            top_y();
        }

        //JUST DO IT
        void xy() {
            try {
                //x left
                if (massive[x][y] == 0) {
                    massive[x][y] = 1;
                    setBackgroundOn(size_of_intro, x, y);
                } else if (massive[x][y] == 1) {
                    massive[x][y] = 0;
                    setBackgroundOff(size_of_intro, x, y);
                }
            } catch (ArrayIndexOutOfBoundsException exception) {
                //noting to do
            }
        }

        void left_x() {
            try {
                //x left
                if (massive[x - 1][y] == 0) {
                    massive[x - 1][y] = 1;
                    setBackgroundOn(size_of_intro, x - 1, y);
                } else if (massive[x - 1][y] == 1) {
                    massive[x - 1][y] = 0;
                    setBackgroundOff(size_of_intro, x - 1, y);
                }
            } catch (ArrayIndexOutOfBoundsException exception) {
                //noting to do
            }
        }

        void right_x() {
            try {
                //x right
                if (massive[x + 1][y] == 0) {
                    massive[x + 1][y] = 1;
                    setBackgroundOn(size_of_intro, x + 1, y);
                } else if (massive[x + 1][y] == 1) {
                    massive[x + 1][y] = 0;
                    setBackgroundOff(size_of_intro, x + 1, y);
                }
            } catch (ArrayIndexOutOfBoundsException exception) {
                //noting to do
            }
        }

        void top_y() {
            try {
                //y top
                if (massive[x][y - 1] == 0) {
                    massive[x][y - 1] = 1;
                    setBackgroundOn(size_of_intro, x, y - 1);
                } else if (massive[x][y - 1] == 1) {
                    massive[x][y - 1] = 0;
                    setBackgroundOff(size_of_intro, x, y - 1);
                }
            } catch (ArrayIndexOutOfBoundsException exception) {
                //noting to do
            }
        }

        void bottom_y() {
            try {
                //y bottom
                if (massive[x][y + 1] == 0) {
                    massive[x][y + 1] = 1;
                    setBackgroundOn(size_of_intro, x, y + 1);
                } else if (massive[x][y + 1] == 1) {
                    massive[x][y + 1] = 0;
                    setBackgroundOff(size_of_intro, x, y + 1);
                }
            } catch (ArrayIndexOutOfBoundsException exception) {
                //noting to do
            }
        }
    }
}
