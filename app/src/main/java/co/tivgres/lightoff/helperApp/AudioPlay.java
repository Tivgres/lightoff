package co.tivgres.lightoff.helperApp;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.util.Log;

import co.tivgres.lightoff.R;

/**
 * Created by srg on 09.10.2016.
 */

public class AudioPlay extends Service {
    private static final String TAG = "AudioPlay";
    public static boolean back;
    MediaPlayer myPlayer;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        //Toast.makeText(this, "My Service Created", Toast.LENGTH_LONG).show();
        Log.d(TAG, "onCreate");
        if (myPlayer != null) {
            myPlayer.release();
        } else if (myPlayer == null) {
            myPlayer = new MediaPlayer();
            if (back) {
                myPlayer = MediaPlayer.create(this, R.raw.play_back);
                myPlayer.setLooping(true); // Set looping
                myPlayer.setVolume(7, 7);
            } else if (!back) {
                myPlayer = MediaPlayer.create(this, R.raw.press);
                myPlayer.setLooping(false); // Set looping
                myPlayer.setVolume(7, 7);
            }
        }
    }

    @Override
    public void onDestroy() {
        //Toast.makeText(this, "My Service Stopped", Toast.LENGTH_LONG).show();
        Log.d(TAG, "onDestroy");
        myPlayer.stop();
        myPlayer.release();
    }


    @Override
    public void onStart(Intent intent, int startid) {
        //Toast.makeText(this, "My Service Started", Toast.LENGTH_LONG).show();
        Log.d(TAG, "onStart");
        myPlayer.start();

    }
}