package co.tivgres.lightoff.helperApp;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.ShareCompat;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.EditText;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import java.util.Locale;

import co.tivgres.lightoff.GameActivity;
import co.tivgres.lightoff.MainActivity;
import co.tivgres.lightoff.R;
import co.tivgres.lightoff.IntroLevelActivity;
import co.tivgres.lightoff.main.FeedbackFragment;
import co.tivgres.lightoff.main.MoreAppFragment;
import co.tivgres.lightoff.main.SelectStageFragment;
import co.tivgres.lightoff.main.SettingsFragment;
import co.tivgres.lightoff.main.StatisticsFragment;

import static android.R.id.message;
import static android.content.Context.MODE_PRIVATE;
import static co.tivgres.lightoff.MainActivity.mainFrag;
import static co.tivgres.lightoff.R.string.game;
import static co.tivgres.lightoff.main.FeedbackFragment.title;
import static co.tivgres.lightoff.main.SelectStageFragment.sizeAreaGame;
import static co.tivgres.lightoff.main.SelectStageFragment.state_bottom;
import static co.tivgres.lightoff.main.SelectStageFragment.state_center;
import static co.tivgres.lightoff.main.SelectStageFragment.state_end;
import static co.tivgres.lightoff.main.SelectStageFragment.state_start;
import static co.tivgres.lightoff.main.SelectStageFragment.state_top;

/**
 * ______________________________
 * What is this project?
 * Project: lightoff
 * ______________________________
 * Who created this?
 * User: tivgres
 * E-mail: mr.serg.vit@gmail.com
 * ______________________________
 * When was created?
 * Date: 14/Apr/2017
 * Time: 16:30
 * ______________________________
 */

public class OnClickListener implements View.OnClickListener {

    private Activity activ;
    private FragmentTransaction ft;
    private InterstitialAd mInterstitialAd;

    public OnClickListener(Activity activity) {
        activ = activity;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_game:
                ft = activ.getFragmentManager().beginTransaction();
                ft.replace(R.id.frame_layout, new SelectStageFragment(), "FRAG_STAGE");
                ft.commit();
                mainFrag = false;
                break;
            case R.id.btn_settings:
                ft = activ.getFragmentManager().beginTransaction();
                ft.replace(R.id.frame_layout, new SettingsFragment(), "FRAG_SETTINGS");
                ft.commit();
                mainFrag = false;
                break;
            case R.id.btn_statistics:
                ft = activ.getFragmentManager().beginTransaction();
                ft.replace(R.id.frame_layout, new StatisticsFragment(), "FRAG_STATISTICS");
                ft.commit();
                mainFrag = false;
                break;
            case R.id.btn_more_app:
                ft = activ.getFragmentManager().beginTransaction();
                ft.replace(R.id.frame_layout, new MoreAppFragment(), "FRAG_MORE_APP");
                ft.commit();
                mainFrag = false;
                break;
            case R.id.btn_feedback:
                ft = activ.getFragmentManager().beginTransaction();
                ft.replace(R.id.frame_layout, new FeedbackFragment(), "FRAG_FEEDBACK");
                ft.commit();
                mainFrag = false;
                break;
            case R.id.btn_intro:
                Intent intro = new Intent(activ, IntroLevelActivity.class);
                activ.startActivity(intro);
                break;
            case R.id.btn_play:
                mInterstitialAd = new InterstitialAd(activ);
                mInterstitialAd.setAdUnitId("ca-app-pub-1265152708850017/5702297680");
                requestNewInterstitial();
                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                    mInterstitialAd.setAdListener(new AdListener() {
                        @Override
                        public void onAdClosed() {
                            requestNewInterstitial();
                            Intent game = new Intent(activ, GameActivity.class);
                            game.putExtra("info", detectStyleOfGame());
                            activ.startActivity(game);
                        }
                    });
                } else if (!mInterstitialAd.isLoaded()) {
                    Intent game = new Intent(activ, GameActivity.class);
                    game.putExtra("info", detectStyleOfGame());
                    activ.startActivity(game);
                }
                break;
            case R.id.btn_remove_ads:
                Intent full_ver = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=co.tivgres.lightoff.plus"));
                activ.startActivity(full_ver);
                break;
            case R.id.btn_select_lang:
                AlertDialog.Builder adLang = new AlertDialog.Builder(activ);
                adLang.setTitle(activ.getResources().getString(R.string.select_lang) + "\n" + activ.getResources().getString(R.string.just_for_app));
                adLang.setItems(activ.getResources().getStringArray(R.array.array_lang), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SharedPreferences.Editor editor = activ.getSharedPreferences("settings", MODE_PRIVATE).edit();
                        if (which == 0) {
                            editor.putString("locate", "en");
                            editor.apply();
                        } else if (which == 1) {
                            editor.putString("locate", "ru");
                            editor.apply();
                        } else if (which == 2) {
                            editor.putString("locate", "uk");
                            editor.apply();
                        } else if (which == 3) {
                            editor.putString("locate", "de");
                            editor.apply();
                        } else if (which == 4) {
                            editor.putString("locate", "fr");
                            editor.apply();
                        } else if (which == 5) {
                            editor.putString("locate", "es");
                            editor.apply();
                        } else if (which == 6) {
                            editor.putString("locate", "pt");
                            editor.apply();
                        } else if (which == 7) {
                            editor.putString("locate", "ar");
                            editor.apply();
                        } else if (which == 8) {
                            editor.putString("locate", "bn");
                            editor.apply();
                        } else if (which == 9) {
                            editor.putString("locate", "hi");
                            editor.apply();
                        } else if (which == 10) {
                            editor.putString("locate", "ja");
                            editor.apply();
                        } else if (which == 11) {
                            editor.putString("locate", "ko");
                            editor.apply();
                        } else if (which == 12) {
                            editor.putString("locate", "zh");
                            editor.apply();
                        }
                        SharedPreferences sharedPreferences = activ.getSharedPreferences("settings", MODE_PRIVATE);
                        String lang = sharedPreferences.getString("locate", "en");
                        setLocale(lang);
                    }
                });
                adLang.create();
                adLang.show();
                break;
            case R.id.btn_about_app:
                AlertDialog.Builder adAbout = new AlertDialog.Builder(activ);
                adAbout.setTitle(R.string.about_app);
                adAbout.setMessage(R.string.about_app_text);
                adAbout.setNeutralButton(activ.getResources().getString(R.string.close), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                adAbout.create();
                adAbout.show();
                break;
            case R.id.btn_play_market:
                Intent gaps = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/developer?id=Tivgres.co"));
                activ.startActivity(gaps);
                break;
            case R.id.btn_site:
                Intent site = new Intent(Intent.ACTION_VIEW, Uri.parse("https://vk.com/tivgres_co"));
                activ.startActivity(site);
                break;
            case R.id.btn_send_email:
                EditText etMessage = (EditText) activ.findViewById(R.id.et_message);
                if (!etMessage.getText().toString().isEmpty()) {
                    ShareCompat.IntentBuilder.from(activ)
                            .setType("message/rfc822")
                            .addEmailTo("info.tivgres.co@gmail.com")
                            .setSubject(title + " - [LightOFF]")
                            .setText(etMessage.getText().toString() + sysInfoForFeedback())
                            //.setHtmlText(body) //If you are using HTML in your body text
                            .setChooserTitle("Choose client:")
                            .startChooser();
                } else {
                    AlertDialog.Builder ad_error = new AlertDialog.Builder(activ);
                    ad_error.setTitle(activ.getResources().getString(R.string.attention));
                    ad_error.setMessage(activ.getResources().getString(R.string.error_send_feedback));
                    ad_error.setNeutralButton(R.string.close, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int arg1) {
                        }
                    });
                    ad_error.create();
                    ad_error.show();
                }
                break;

        }
    }

    private String sysInfoForFeedback() {
        return "\n" + "This information does not contain the confidentiality of data, and we need it as soon as possible to resolve the problem." + "\n"
                + "Model: " + Build.MODEL + "\n"
                + "OS_ver: " + Build.VERSION.RELEASE + "\n"
                + "MANUFACTURER: " + Build.MANUFACTURER + "\n"
                + "Product: " + Build.PRODUCT + "\n"
                + "Display: " + Build.DISPLAY;
    }

    private void requestNewInterstitial() {
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice("F50224401BBD7FB5DB11D9478C33EF52")
                .addTestDevice("81B910A14B82F82EFDE5A4CCEA565793")
                .addTestDevice("86E62348E1A4C03533C0415BA565B84C")
                .addTestDevice("2BB6D40DDA3718490E59B69010B84079")
                .build();

        mInterstitialAd.loadAd(adRequest);
    }

    private int[] detectStyleOfGame() {
        int[] style = new int[6];
        //detect size
        if (sizeAreaGame == 0) {
            style[0] = 3;
        } else if (sizeAreaGame == 20) {
            style[0] = 5;
        } else if (sizeAreaGame == 40) {
            style[0] = 6;
        } else if (sizeAreaGame == 60) {
            style[0] = 7;
        } else if (sizeAreaGame == 80) {
            style[0] = 8;
        } else if (sizeAreaGame == 100) {
            style[0] = 9;
        }
        //detect form
        if (state_top)
            style[1] = 1;
        else
            style[1] = 0;
        if (state_start)
            style[2] = 1;
        else
            style[2] = 0;
        if (state_center)
            style[3] = 1;
        else
            style[3] = 0;
        if (state_end)
            style[4] = 1;
        else
            style[4] = 0;
        if (state_bottom)
            style[5] = 1;
        else
            style[5] = 0;
        return style;
    }

    public void setLocale(String lang) {
        Locale myLocale = new Locale(lang);
        Resources res = activ.getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
        Intent refresh = new Intent(activ, MainActivity.class);
        activ.finish();
        activ.startActivity(refresh);
    }
}
