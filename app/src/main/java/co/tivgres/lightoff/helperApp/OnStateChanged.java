package co.tivgres.lightoff.helperApp;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Toast;

import co.tivgres.lightoff.R;

import static android.content.Context.MODE_PRIVATE;
import static co.tivgres.lightoff.main.SelectStageFragment.state_top;
import static co.tivgres.lightoff.main.SelectStageFragment.state_start;
import static co.tivgres.lightoff.main.SelectStageFragment.state_center;
import static co.tivgres.lightoff.main.SelectStageFragment.state_end;
import static co.tivgres.lightoff.main.SelectStageFragment.state_bottom;
import static co.tivgres.lightoff.helperApp.AudioPlay.back;

/**
 * ______________________________
 * What is this project?
 * Project: lightoff
 * ______________________________
 * Who created this?
 * User: tivgres
 * E-mail: mr.serg.vit@gmail.com
 * ______________________________
 * When was created?
 * Date: 16/Apr/2017
 * Time: 01:47
 * ______________________________
 */

public class OnStateChanged implements OnCheckedChangeListener {

    private Activity activ;

    public OnStateChanged(Activity activity) {
        activ = activity;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        SharedPreferences.Editor sh_sys_ed = activ.getSharedPreferences("sys_info", MODE_PRIVATE).edit();
        switch (buttonView.getId()) {
            case R.id.tg_lamp_top:
                if (isChecked) {
                    state_top = true;
                    sh_sys_ed.putBoolean("tg_top", true);
                } else if (!isChecked) {
                    if (!checkMinState()) {
                        buttonView.setChecked(true);
                        showAdErr();
                    } else {
                        state_top = false;
                        sh_sys_ed.putBoolean("tg_top", false);
                    }
                }
                break;
            case R.id.tg_lamp_start:
                if (isChecked) {
                    state_start = true;
                    sh_sys_ed.putBoolean("tg_start", true);
                } else if (!isChecked) {
                    if (!checkMinState()) {
                        buttonView.setChecked(true);
                        showAdErr();
                    } else {
                        state_start = false;
                        sh_sys_ed.putBoolean("tg_start", false);
                    }
                }
                break;
            case R.id.tg_lamp_center:
                if (isChecked) {
                    state_center = true;
                    sh_sys_ed.putBoolean("tg_center", true);
                } else if (!isChecked) {
                    if (!checkMinState()) {
                        buttonView.setChecked(true);
                        showAdErr();
                    } else {
                        state_center = false;
                        sh_sys_ed.putBoolean("tg_center", false);
                    }
                }
                break;
            case R.id.tg_lamp_end:
                if (isChecked) {
                    state_end = true;
                    sh_sys_ed.putBoolean("tg_end", true);
                } else if (!isChecked) {
                    if (!checkMinState()) {
                        buttonView.setChecked(true);
                        showAdErr();
                    } else {
                        state_end = false;
                        sh_sys_ed.putBoolean("tg_end", false);
                    }
                }
                break;
            case R.id.tg_lamp_bottom:
                if (isChecked) {
                    state_bottom = true;
                    sh_sys_ed.putBoolean("tg_bottom", true);
                } else if (!isChecked) {
                    if (!checkMinState()) {
                        buttonView.setChecked(true);
                        showAdErr();
                    } else {
                        state_bottom = false;
                        sh_sys_ed.putBoolean("tg_bottom", false);
                    }
                }
                break;
            case R.id.tg_btn_music:
                if (isChecked) {
                    SharedPreferences.Editor editor = activ.getSharedPreferences("settings", MODE_PRIVATE).edit();
                    editor.putBoolean("music", true);
                    editor.apply();
                    back = true;
                    activ.startService(new Intent(activ, AudioPlay.class));
                    Toast.makeText(activ, R.string.music_enabled,
                            Toast.LENGTH_SHORT).show();
                } else if (!isChecked) {
                    SharedPreferences.Editor editor = activ.getSharedPreferences("settings", MODE_PRIVATE).edit();
                    editor.putBoolean("music", false);
                    editor.apply();
                    activ.stopService(new Intent(activ, AudioPlay.class));
                    Toast.makeText(activ, R.string.music_disabled,
                            Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.tg_btn_sound:
                if (isChecked) {
                    SharedPreferences.Editor editor = activ.getSharedPreferences("settings", MODE_PRIVATE).edit();
                    editor.putBoolean("sound", true);
                    editor.apply();
                    Toast.makeText(activ, R.string.sounds_enabled,
                            Toast.LENGTH_SHORT).show();
                } else if (!isChecked) {
                    SharedPreferences.Editor editor = activ.getSharedPreferences("settings", MODE_PRIVATE).edit();
                    editor.putBoolean("sound", false);
                    editor.apply();
                    Toast.makeText(activ, R.string.sounds_disabled,
                            Toast.LENGTH_SHORT).show();
                }
                break;
        }
        sh_sys_ed.apply();
    }

    private void showAdErr() {
        final AlertDialog.Builder adErr = new AlertDialog.Builder(activ);
        adErr.setTitle("Error!");
        adErr.setMessage("Lamps must be switched on >= 3");
        adErr.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        adErr.create();
        adErr.show();
    }

    private boolean checkMinState() {
        int count = 0;
        boolean[] states = {state_top, state_start, state_center, state_end, state_bottom};
        for (boolean state : states) {
            if (state) {
                count++;
            }
        }
        return count > 3;
    }
}
