package co.tivgres.lightoff;

/**
 * ______________________________
 * What is this project?
 * Project: lightoff
 * ______________________________
 * Who created this?
 * User: tivgres
 * E-mail: mr.serg.vit@gmail.com
 * ______________________________
 * When was created?
 * Date: 14/Apr/2017
 * Time: 15:54
 * ______________________________
 */

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.app.FragmentTransaction;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;


import java.util.Locale;
import java.util.Random;

import co.tivgres.lightoff.helperApp.AudioPlay;
import co.tivgres.lightoff.main.MainFragment;

import static co.tivgres.lightoff.helperApp.AudioPlay.back;

public class MainActivity extends AppCompatActivity {
    public static boolean mainFrag = true;

    final String LOG_TAG = "Logs";
    SharedPreferences sharedPreferences;
    int rank;
    private long backPressedTime = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPreferences = getSharedPreferences("settings", MODE_PRIVATE);
        boolean shared_exist = sharedPreferences.getBoolean("shared_exist", false);
        if (!shared_exist) {
            Random random = new Random();
            rank = 99777 + random.nextInt(2222);
            shared_settings_put();
            shared_statistics_put();
        }
        //set locate android
        Locale locale = new Locale(sharedPreferences.getString("locate", "en"));
        Locale.setDefault(locale);
        Configuration config = getBaseContext().getResources().getConfiguration();
        config.setLocale(locale);
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());
        setContentView(R.layout.activity_main);
        // set content

        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.frame_layout, new MainFragment(), "FRAG_MAIN");
        ft.commit();

        //Full screen mode
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    @Override
    protected void onResume() {
        super.onResume();
        sharedPreferences = getSharedPreferences("settings", MODE_PRIVATE);
        if (sharedPreferences.getBoolean("music", true)) {
            back = true;
            startService(new Intent(MainActivity.this, AudioPlay.class));
        }
        Log.d(LOG_TAG, "onResume ");
    }


    @Override
    protected void onPause() {
        super.onPause();
        stopService(new Intent(MainActivity.this, AudioPlay.class));
        Log.d(LOG_TAG, "onPause ");
    }

    protected void onStop() {
        super.onStop();
    }




    public void onBackPressed() {
        if (!mainFrag) {
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.replace(R.id.frame_layout, new MainFragment(), "FRAG_MAIN");
            ft.commit();
            mainFrag = true;
        } else {
            long t = System.currentTimeMillis();
            if (t - backPressedTime > 2000) {    // 2 secs
                backPressedTime = t;
                Toast toast = Toast.makeText(getApplicationContext(),
                        R.string.exit, Toast.LENGTH_SHORT);
                toast.show();
            } else {    // this guy is serious
                // clean up
                super.onBackPressed();       // bye
            }
        }
    }

    public void shared_statistics_put() {
        SharedPreferences.Editor editor = getSharedPreferences("statistics", MODE_PRIVATE).edit();
        editor.putInt("points", 0);
        editor.apply();
        editor.putLong("tap_all", 0);
        editor.apply();
        editor.putInt("tap_min", 9999);
        editor.apply();
        editor.putInt("tap_max", 0);
        editor.apply();
        editor.putInt("level", 1);
        editor.apply();
        editor.putLong("time_in_app", 0);
        editor.apply();
        editor.putInt("rank", rank);
        editor.apply();
    }

    public void shared_settings_put() {
        String current = Locale.getDefault().getLanguage();
        SharedPreferences.Editor editor = getSharedPreferences("settings", MODE_PRIVATE).edit();
        editor.putBoolean("shared_exist", true);
        editor.apply();
        editor.putBoolean("music", true);
        editor.apply();
        editor.putBoolean("sound", true);
        editor.apply();
        editor.putString("locate", current);
        editor.apply();
        editor.putString("locate_current", current);
        editor.apply();
    }


    @Override
    public void onStart() {
        super.onStart();

    }
}