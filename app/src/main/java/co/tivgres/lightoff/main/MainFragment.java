package co.tivgres.lightoff.main;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import co.tivgres.lightoff.helperApp.OnClickListener;
import co.tivgres.lightoff.R;

/**
 * ______________________________
 * What is this project?
 * Project: lightoff
 * ______________________________
 * Who created this?
 * User: tivgres
 * E-mail: mr.serg.vit@gmail.com
 * ______________________________
 * When was created?
 * Date: 14/Apr/2017
 * Time: 15:54
 * ______________________________
 */

public class MainFragment extends Fragment {

    Button btnGame, btnSettings, btnStatistics, btnMoreApp, btnFeedback;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        OnClickListener ocl = new OnClickListener(getActivity());
        btnGame = (Button) getActivity().findViewById(R.id.btn_game);
        btnGame.setOnClickListener(ocl);
        btnSettings = (Button) getActivity().findViewById(R.id.btn_settings);
        btnSettings.setOnClickListener(ocl);
        btnStatistics = (Button) getActivity().findViewById(R.id.btn_statistics);
        btnStatistics.setOnClickListener(ocl);
        btnMoreApp = (Button) getActivity().findViewById(R.id.btn_more_app);
        btnMoreApp.setOnClickListener(ocl);
        btnFeedback = (Button) getActivity().findViewById(R.id.btn_feedback);
        btnFeedback.setOnClickListener(ocl);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
