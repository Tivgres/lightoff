package co.tivgres.lightoff.main;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.ToggleButton;

import co.tivgres.lightoff.R;
import co.tivgres.lightoff.helperApp.OnClickListener;
import co.tivgres.lightoff.helperApp.OnStateChanged;

/**
 * ______________________________
 * What is this project?
 * Project: lightoff
 * ______________________________
 * Who created this?
 * User: tivgres
 * E-mail: mr.serg.vit@gmail.com
 * ______________________________
 * When was created?
 * Date: 14/Apr/2017
 * Time: 15:56
 * ______________________________
 */

public class SelectStageFragment extends Fragment {

    public static boolean state_top, state_start, state_center, state_end, state_bottom;
    public static int sizeAreaGame;
    private TextView tv_3x3, tv_5x5, tv_6x6, tv_7x7, tv_8x8, tv_9x9;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_choose_level, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        OnClickListener ocl = new OnClickListener(getActivity());
        Button btn_intro = (Button) getView().findViewById(R.id.btn_intro);
        btn_intro.setOnClickListener(ocl);
        Button btn_play = (Button) getView().findViewById(R.id.btn_play);
        btn_play.setOnClickListener(ocl);
        tv_3x3 = (TextView) getView().findViewById(R.id.tv_lvl3);
        tv_5x5 = (TextView) getView().findViewById(R.id.tv_lvl5);
        tv_6x6 = (TextView) getView().findViewById(R.id.tv_lvl6);
        tv_7x7 = (TextView) getView().findViewById(R.id.tv_lvl7);
        tv_8x8 = (TextView) getView().findViewById(R.id.tv_lvl8);
        tv_9x9 = (TextView) getView().findViewById(R.id.tv_lvl9);
        SeekBar sb_chooser = (SeekBar) getView().findViewById(R.id.sb_chooser);
        SharedPreferences sh_sys = getActivity().getSharedPreferences("sys_info", Context.MODE_PRIVATE);
        sb_chooser.setProgress(sh_sys.getInt("last_level", 0));
        sizeAreaGame = sh_sys.getInt("last_level", 0);
        setColorTextView(sh_sys.getInt("last_level", 0));
        sb_chooser.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                int progress = seekBar.getProgress();
                if (progress <= 10) {
                    seekBar.setProgress(0);
                } else if (progress >= 11 && progress <= 30) {
                    seekBar.setProgress(20);
                } else if (progress >= 31 && progress <= 50) {
                    seekBar.setProgress(40);
                } else if (progress >= 51 && progress <= 70) {
                    seekBar.setProgress(60);
                } else if (progress >= 71 && progress <= 90) {
                    seekBar.setProgress(80);
                } else if (progress >= 91) {
                    seekBar.setProgress(100);
                }
                SharedPreferences.Editor sh_sys_ed = getActivity().getSharedPreferences("sys_info", Context.MODE_PRIVATE).edit();
                sh_sys_ed.putInt("last_level", seekBar.getProgress());
                sh_sys_ed.apply();
                sizeAreaGame = seekBar.getProgress();
                setColorTextView(seekBar.getProgress());

            }
        });
        OnStateChanged osc = new OnStateChanged(getActivity());
        ToggleButton tg_top = (ToggleButton) getView().findViewById(R.id.tg_lamp_top);
        tg_top.setChecked(sh_sys.getBoolean("tg_top", true));
        state_top = tg_top.isChecked();
        tg_top.setOnCheckedChangeListener(osc);
        ToggleButton tg_start = (ToggleButton) getView().findViewById(R.id.tg_lamp_start);
        tg_start.setChecked(sh_sys.getBoolean("tg_start", true));
        state_start = tg_start.isChecked();
        tg_start.setOnCheckedChangeListener(osc);
        ToggleButton tg_center = (ToggleButton) getView().findViewById(R.id.tg_lamp_center);
        tg_center.setChecked(sh_sys.getBoolean("tg_center", true));
        state_center = tg_center.isChecked();
        tg_center.setOnCheckedChangeListener(osc);
        ToggleButton tg_end = (ToggleButton) getView().findViewById(R.id.tg_lamp_end);
        tg_end.setChecked(sh_sys.getBoolean("tg_end", true));
        state_end = tg_end.isChecked();
        tg_end.setOnCheckedChangeListener(osc);
        ToggleButton tg_bottom = (ToggleButton) getView().findViewById(R.id.tg_lamp_bottom);
        tg_bottom.setChecked(sh_sys.getBoolean("tg_bottom", true));
        state_bottom = tg_bottom.isChecked();
        tg_bottom.setOnCheckedChangeListener(osc);
    }

    private void setColorTextView(int progress) {
        if (progress <= 10) {
            tv_3x3.setTextColor(getResources().getColor(R.color.red_selected));
            tv_5x5.setTextColor(getResources().getColor(R.color.text));
            tv_6x6.setTextColor(getResources().getColor(R.color.text));
            tv_7x7.setTextColor(getResources().getColor(R.color.text));
            tv_8x8.setTextColor(getResources().getColor(R.color.text));
            tv_9x9.setTextColor(getResources().getColor(R.color.text));
        } else if (progress >= 11 && progress <= 30) {
            tv_3x3.setTextColor(getResources().getColor(R.color.text));
            tv_5x5.setTextColor(getResources().getColor(R.color.red_selected));
            tv_6x6.setTextColor(getResources().getColor(R.color.text));
            tv_7x7.setTextColor(getResources().getColor(R.color.text));
            tv_8x8.setTextColor(getResources().getColor(R.color.text));
            tv_9x9.setTextColor(getResources().getColor(R.color.text));
        } else if (progress >= 31 && progress <= 50) {
            tv_3x3.setTextColor(getResources().getColor(R.color.text));
            tv_5x5.setTextColor(getResources().getColor(R.color.text));
            tv_6x6.setTextColor(getResources().getColor(R.color.red_selected));
            tv_7x7.setTextColor(getResources().getColor(R.color.text));
            tv_8x8.setTextColor(getResources().getColor(R.color.text));
            tv_9x9.setTextColor(getResources().getColor(R.color.text));
        } else if (progress >= 51 && progress <= 70) {
            tv_3x3.setTextColor(getResources().getColor(R.color.text));
            tv_5x5.setTextColor(getResources().getColor(R.color.text));
            tv_6x6.setTextColor(getResources().getColor(R.color.text));
            tv_7x7.setTextColor(getResources().getColor(R.color.red_selected));
            tv_8x8.setTextColor(getResources().getColor(R.color.text));
            tv_9x9.setTextColor(getResources().getColor(R.color.text));
        } else if (progress >= 71 && progress <= 90) {
            tv_3x3.setTextColor(getResources().getColor(R.color.text));
            tv_5x5.setTextColor(getResources().getColor(R.color.text));
            tv_6x6.setTextColor(getResources().getColor(R.color.text));
            tv_7x7.setTextColor(getResources().getColor(R.color.text));
            tv_8x8.setTextColor(getResources().getColor(R.color.red_selected));
            tv_9x9.setTextColor(getResources().getColor(R.color.text));
        } else if (progress >= 91) {
            tv_3x3.setTextColor(getResources().getColor(R.color.text));
            tv_5x5.setTextColor(getResources().getColor(R.color.text));
            tv_6x6.setTextColor(getResources().getColor(R.color.text));
            tv_7x7.setTextColor(getResources().getColor(R.color.text));
            tv_8x8.setTextColor(getResources().getColor(R.color.text));
            tv_9x9.setTextColor(getResources().getColor(R.color.red_selected));
        }
    }

}
