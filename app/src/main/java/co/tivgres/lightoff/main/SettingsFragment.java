package co.tivgres.lightoff.main;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ToggleButton;

import co.tivgres.lightoff.R;
import co.tivgres.lightoff.helperApp.OnClickListener;
import co.tivgres.lightoff.helperApp.OnStateChanged;

/**
 * ______________________________
 * What is this project?
 * Project: lightoff
 * ______________________________
 * Who created this?
 * User: tivgres
 * E-mail: mr.serg.vit@gmail.com
 * ______________________________
 * When was created?
 * Date: 14/Apr/2017
 * Time: 15:55
 * ______________________________
 */

public class SettingsFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_settings, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        OnClickListener ocl = new OnClickListener(getActivity());
        Button btn_remove_ads = (Button) getView().findViewById(R.id.btn_remove_ads);
        btn_remove_ads.setOnClickListener(ocl);
        Button btn_language = (Button) getView().findViewById(R.id.btn_select_lang);
        btn_language.setOnClickListener(ocl);
        Button btn_about_app = (Button) getView().findViewById(R.id.btn_about_app);
        btn_about_app.setOnClickListener(ocl);
        OnStateChanged osc = new OnStateChanged(getActivity());
        ToggleButton tg_btn_music = (ToggleButton) getView().findViewById(R.id.tg_btn_music);
        tg_btn_music.setOnCheckedChangeListener(osc);
        ToggleButton tg_btn_sound = (ToggleButton) getView().findViewById(R.id.tg_btn_sound);
        tg_btn_sound.setOnCheckedChangeListener(osc);
    }
}
