package co.tivgres.lightoff.main;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import co.tivgres.lightoff.R;
import co.tivgres.lightoff.helperApp.OnClickListener;

/**
 * ______________________________
 * What is this project?
 * Project: lightoff
 * ______________________________
 * Who created this?
 * User: tivgres
 * E-mail: mr.serg.vit@gmail.com
 * ______________________________
 * When was created?
 * Date: 14/Apr/2017
 * Time: 17:05
 * ______________________________
 */

public class FeedbackFragment extends Fragment {

    private String[] titlesAdapter = {"Offer", "Question", "Error software", "Error design"};
    public static String title;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_feedback, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Spinner spinner_title = (Spinner) getView().findViewById(R.id.spinner_title);
        spinner_title.setAdapter(new ArrayAdapter<>(getActivity(), R.layout.spinner_item, titlesAdapter));
        spinner_title.setSelection(1);
        spinner_title.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                title = titlesAdapter[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                title = titlesAdapter[0];
            }
        });
        Button btn_send_feedback = (Button) getView().findViewById(R.id.btn_send_email);
        btn_send_feedback.setOnClickListener(new OnClickListener(getActivity()));
    }
}
