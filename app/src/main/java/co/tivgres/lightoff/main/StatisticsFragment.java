package co.tivgres.lightoff.main;

import android.app.Fragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import co.tivgres.lightoff.R;

import static android.content.Context.MODE_PRIVATE;
import static android.icu.lang.UCharacter.GraphemeClusterBreak.L;

/**
 * ______________________________
 * What is this project?
 * Project: lightoff
 * ______________________________
 * Who created this?
 * User: tivgres
 * E-mail: mr.serg.vit@gmail.com
 * ______________________________
 * When was created?
 * Date: 14/Apr/2017
 * Time: 15:57
 * ______________________________
 */

public class StatisticsFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_statistics, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getStatAndSet();
    }

    private void getStatAndSet() {
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("statistics", MODE_PRIVATE);

        TextView all_taps = (TextView) getView().findViewById(R.id.tv_statistics_tap_all_value);
        String s_all_taps = Long.toString(sharedPreferences.getLong("tap_all", 0));
        all_taps.setText(s_all_taps);


        TextView min_taps = (TextView) getView().findViewById(R.id.tv_statistics_tap_min_value);
        String s_min_taps = Integer.toString(sharedPreferences.getInt("tap_min", 0));
        if (s_min_taps.equals("9999")) {
            min_taps.setText("-");
        } else if (!s_min_taps.equals("9999")) {
            min_taps.setText(s_min_taps);
        }

        TextView max_taps = (TextView) getView().findViewById(R.id.tv_statistics_tap_max_value);
        String s_max_taps = Integer.toString(sharedPreferences.getInt("tap_max", 0));
        if (s_max_taps.equals("0")) {
            max_taps.setText("-");
        } else if (!s_max_taps.equals("0")) {
            max_taps.setText(s_max_taps);
        }


        TextView time_in_app = (TextView) getView().findViewById(R.id.tv_statistics_time_in_app_value);
        long timeInApp = (sharedPreferences.getLong("time_in_app", 0));
        int hour = (int) (timeInApp / 3600);
        int min = (int) (timeInApp / 60) - (hour * 60);
        int sec = (int) (timeInApp - (hour * 3600) - (min * 60));
        time_in_app.setText(String.valueOf(hour + ":" + min + ":" + sec));

    }
}
