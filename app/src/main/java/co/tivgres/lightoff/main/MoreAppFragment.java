package co.tivgres.lightoff.main;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import co.tivgres.lightoff.R;
import co.tivgres.lightoff.helperApp.OnClickListener;

/**
 * ______________________________
 * What is this project?
 * Project: lightoff
 * ______________________________
 * Who created this?
 * User: tivgres
 * E-mail: mr.serg.vit@gmail.com
 * ______________________________
 * When was created?
 * Date: 14/Apr/2017
 * Time: 15:56
 * ______________________________
 */

public class MoreAppFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_more_app, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        OnClickListener ocl = new OnClickListener(getActivity());
        Button btn_gp = (Button) getView().findViewById(R.id.btn_play_market);
        btn_gp.setOnClickListener(ocl);
        Button btn_site = (Button) getView().findViewById(R.id.btn_site);
        btn_site.setOnClickListener(ocl);
    }
}
