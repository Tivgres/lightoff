package co.tivgres.lightoff;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.util.Arrays;
import java.util.Random;

import co.tivgres.lightoff.helperApp.AudioPlay;
import co.tivgres.lightoff.helperGame.CalculateStatistics;
import co.tivgres.lightoff.helperGame.Game;
import co.tivgres.lightoff.helperGame.Reset_field;

import static co.tivgres.lightoff.helperApp.AudioPlay.back;
import static co.tivgres.lightoff.R.id.btn_reload;

/**
 * Created by tivgres on 1/7/17.
 */

public class GameActivity extends Activity {
    /*object*/

    //allert
    AlertDialog.Builder ad_reset;
    AlertDialog.Builder ad_success;
    Context context;
    //generate mass
    Game game = new Game();
    //btn
    public Button button;
    //calc stat
    CalculateStatistics cs;
    //random for rank
    Random random = new Random();
    int min_rand = 95333;
    int start_rank = random.nextInt(4444) + min_rand;

    /*value*/

    //size_field
    int size;
    public static boolean reset = false;
    public static boolean test_mode = false;
    public int[][] mass;
    public int[][] mass_empty;
    //public static boolean frst_lvl_ger = true;
    //for gen btn
    int i;
    int j;
    int lenI;
    int lenJ;
    //for logs
    final String LOG_TAG = "Logs";
    //for statistics
    public static int count = 0;
    public static long start_time = 0;
    public static long pause_time = 0;
    public static long calibration_time = 0;
    public static long resume_time = 0;
    public static long diff_time = 0;
    public static int round_time = 0;
    public int points;
    public int level;
    public long time_in_app;
    public int rank;
    public int min_tap;
    public int max_tap;
    public long all_tap;

    int points_shared;
    long tap_all_shared;
    int tap_min_shared;
    int tap_max_shared;
    int level_shared;
    long time_in_app_shared;
    int rank_shared;
    /*
    *String
    * for
    * success
    */
    String success_alert;
    String success_complete;
    String lvl;
    String count_tap;
    String success_min_tap;
    String success_max_tap;
    String time_round;
    String time_round_h;
    String time_round_m;
    String time_round_s;
    String your_level;
    String level_up;
    String level_down;
    String level_position;
    String you_claim;
    String point;
    String finish;
    String stage_super_easy;
    String stage_easy;
    String stage_medium;
    String stage_hard;
    String stage_super_hard;
    String stage_extreme;
    //for generator string to win dialog
    static String final_text_success;
    static String string_success_points;
    static String string_success_complete_level;
    static String string_success_level;
    static String name_level;
    static String string_success_round_time;
    static String time_of_round;
    static String string_success_tap;
    static String string_success_tap_is_min;
    static String string_success_tap_is_max;

    /*graphics*/

    //create button
    public Button[][] btn;
    //layout
    TableLayout layout;
    TextView tv_tap_in_game;
    TextView tv_time_in_game;


    /*methods*/

    public void get_string() {
        success_alert = getResources().getString(R.string.success_alert);
        success_complete = getResources().getString(R.string.success_complete);
        lvl = getResources().getString(R.string.lvl);
        count_tap = getResources().getString(R.string.count_tap);
        success_min_tap = getResources().getString(R.string.success_min_tap);
        success_max_tap = getResources().getString(R.string.success_max_tap);
        time_round = getResources().getString(R.string.time_round);
        time_round_h = getResources().getString(R.string.time_round_h);
        time_round_m = getResources().getString(R.string.time_round_m);
        time_round_s = getResources().getString(R.string.time_round_s);
        your_level = getResources().getString(R.string.your_level);
        level_up = getResources().getString(R.string.level_up);
        level_down = getResources().getString(R.string.level_down);
        level_position = getResources().getString(R.string.level_position);
        you_claim = getResources().getString(R.string.you_claim);
        point = getResources().getString(R.string.point);
        finish = getResources().getString(R.string.finish);
        stage_super_easy = getResources().getString(R.string.stage_super_easy);
        stage_easy = getResources().getString(R.string.stage_easy);
        stage_medium = getResources().getString(R.string.stage_medium);
        stage_hard = getResources().getString(R.string.stage_hard);
        stage_super_hard = getResources().getString(R.string.stage_super_hard);
        stage_extreme = getResources().getString(R.string.stage_extreme);
    }




    //get size lcd
    public Point Size() {
        Point size_lcd = new Point();
        Display display = getWindowManager().getDefaultDisplay();
        display.getSize(size_lcd);
        return size_lcd;
    }


    //build play area
    public void build_field(int size, boolean reset, boolean test_mode) {
        btn = new Button[size][size];
        mass_empty = new int[size][size];
        mass = game.Get(size, reset, test_mode);
        //set sizes
        int width_lcd = Size().x;
        int height_lcd = Size().y;
        //check orient
        if (width_lcd > height_lcd) {
            width_lcd = height_lcd;
        }
        //calculate sizes
        int width_btn = width_lcd / size;
        int height_btn = width_btn;
        int width_row = width_lcd;
        int height_row = height_btn;
        /*//create button
        Button[][] btn = new Button[size][size];*/
        for (i = 0, lenI = mass.length; i < lenI; i++) {
            TableRow row = new TableRow(this); // create row
            for (j = 0, lenJ = mass[i].length; j < lenJ; j++) {
                button = new Button(this);
                btn[i][j] = button;
                button.setMaxWidth(width_btn);
                button.setWidth(width_btn);
                button.setMinimumWidth(width_btn);
                button.setMaxHeight(height_btn);
                button.setHeight(height_btn);
                button.setMinHeight(height_btn);
                button.setOnClickListener(new Listener(i, j, size, mass)); // add listener
                row.addView(button); // create button
                //set background button
                if (mass[i][j] == 1) {
                    setBackgroundOn(size, i, j);
                }
                if (mass[i][j] == 0) {
                    setBackgroundOff(size, i, j);
                }
            }
            layout.addView(row, new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,
                    TableLayout.LayoutParams.WRAP_CONTENT)); // добавление строки в таблицу
        }
    }

    //set background dynamic for lamp on
    public void setBackgroundOn(int size, int i, int j) {
        if (size == 3) {
            btn[i][j].setBackgroundResource(R.mipmap.lamp_on_v3);
        } else if (size == 5) {
            btn[i][j].setBackgroundResource(R.mipmap.lamp_on_v5);
        } else if (size == 6) {
            btn[i][j].setBackgroundResource(R.mipmap.lamp_on_v6);
        } else if (size == 7) {
            btn[i][j].setBackgroundResource(R.mipmap.lamp_on_v7);
        } else if (size == 8) {
            btn[i][j].setBackgroundResource(R.mipmap.lamp_on_v8);
        } else if (size == 9) {
            btn[i][j].setBackgroundResource(R.mipmap.lamp_on_v9);
        }
    }

    //set background dynamic for lamp off
    public void setBackgroundOff(int size, int i, int j) {
        if (size == 3) {
            btn[i][j].setBackgroundResource(R.mipmap.lamp_off_v3);
        } else if (size == 5) {
            btn[i][j].setBackgroundResource(R.mipmap.lamp_off_v5);
        } else if (size == 6) {
            btn[i][j].setBackgroundResource(R.mipmap.lamp_off_v6);
        } else if (size == 7) {
            btn[i][j].setBackgroundResource(R.mipmap.lamp_off_v7);
        } else if (size == 8) {
            btn[i][j].setBackgroundResource(R.mipmap.lamp_off_v8);
        } else if (size == 9) {
            btn[i][j].setBackgroundResource(R.mipmap.lamp_off_v9);
        }
    }

    public void onClick_reset(View v) {
        Button btn_reload;
        btn_reload = (Button) findViewById(R.id.btn_reload);
        assert btn_reload != null;

        btn_reload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClicked(v);
            }
        });

    }


    public void onClicked(View v) {
        switch (v.getId()) {
            case btn_reload:
                ad_reset.show();
                break;
        }
    }




    public void cacl_value_and_strings() {
        SharedPreferences sharedPreferences = getSharedPreferences("statistics", MODE_PRIVATE);
        time_in_app_shared = sharedPreferences.getLong("time_in_app", 0);
        time_in_app = cs.calc_time_in_app(round_time, time_in_app_shared);
        time_of_round = cs.formatting_time(round_time, time_round_s, time_round_m, time_round_h);
        string_success_round_time = time_round + " " + time_of_round;
        tap_all_shared = sharedPreferences.getLong("tap_all", 0);
        all_tap = cs.calc_all_tap(count, tap_all_shared);
        tap_min_shared = sharedPreferences.getInt("tap_min", 999);
        min_tap = cs.calc_is_min(count, tap_min_shared);
        if (count <= tap_min_shared) {
            string_success_tap_is_min = success_min_tap + " " + count + "\n";
        } else {
            string_success_tap_is_min = "";
        }
        tap_max_shared = sharedPreferences.getInt("tap_max", 0);
        max_tap = cs.calc_is_max(count, tap_max_shared);
        if (count >= tap_max_shared) {
            string_success_tap_is_max = success_max_tap + " " + count + "\n";
        } else {
            string_success_tap_is_max = "";
        }
        string_success_tap = count_tap + ":" + " " + count + "\n";
        //generate all String success
        final_text_success = string_success_tap
                + string_success_round_time
                + string_success_tap_is_min
                + string_success_tap_is_max;
    }

    public void calc_value_and_save() {
        SharedPreferences sharedPreferences = getSharedPreferences("statistics", MODE_PRIVATE);
        SharedPreferences.Editor editor = getSharedPreferences("statistics", MODE_PRIVATE).edit();
        time_in_app_shared = sharedPreferences.getLong("time_in_app", 0);
        time_in_app = cs.calc_time_in_app(round_time, time_in_app_shared);
        editor.putLong("time_in_app", time_in_app);
        tap_all_shared = sharedPreferences.getLong("tap_all", 0);
        all_tap = cs.calc_all_tap(count, tap_all_shared);
        editor.putLong("tap_all", all_tap);
        editor.apply();
        tap_min_shared = sharedPreferences.getInt("tap_min", 999);
        min_tap = cs.calc_is_min(count, tap_min_shared);
        editor.putInt("tap_min", min_tap);
        editor.apply();
        tap_max_shared = sharedPreferences.getInt("tap_max", 0);
        max_tap = cs.calc_is_max(count, tap_max_shared);
        editor.putInt("tap_max", max_tap);
        editor.apply();
    }
    //check win
    public void checker() {
        if (Arrays.deepEquals(mass, mass_empty)) {
            thread_time.interrupt();
            calc_value_and_save();
            ad_success.show();
        }
    }

    public void alert_dialog_reset() {
        context = GameActivity.this;


        ad_reset = new AlertDialog.Builder(context);
        ad_reset.setTitle(R.string.reset_field);  // заголовок
        ad_reset.setMessage(R.string.reset_field_ask); // сообщение
        ad_reset.setPositiveButton(R.string.reset_yes, new OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {
                reset = true;
                resume_time = (long) Math.floor(System.currentTimeMillis() / 1000);
                layout.removeAllViews();
                build_field(size, reset, test_mode);
                tv_tap_in_game.setText(getResources().getString(R.string.count_tap) + ":" + "\n" + "\t" + "\t" + "\t" + count);
                Toast.makeText(context, R.string.reset,
                        Toast.LENGTH_LONG).show();
            }
        });
        ad_reset.setNegativeButton(R.string.reset_no, new OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {
                Toast.makeText(context, R.string.reset_no, Toast.LENGTH_LONG)
                        .show();
            }
        });
        ad_reset.setCancelable(true);
        ad_reset.setOnCancelListener(new OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
                Toast.makeText(context, R.string.cancel,
                        Toast.LENGTH_LONG).show();
            }
        });
        /*ad_reset.setNeutralButton(R.string.level, new OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {
                test_mode = true;
                layout.removeAllViews();
                build_field(size, reset, test_mode);
                Toast.makeText(context, R.string.test_mode,
                        Toast.LENGTH_LONG).show();
            }
        });*/
    }


    public void alert_dialog_success() {

        context = GameActivity.this;
        ad_success = new AlertDialog.Builder(context);
        ad_success.setTitle(success_alert);  // заголовок
        ad_success.setMessage(final_text_success); // сообщение

        ad_success.setPositiveButton(R.string.finish, new OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {
                Reset_field.reset_field();
                resume_time = 0;
                finish();
            }
        });
        ad_success.setCancelable(false);
    }

    //update tv with time
    Thread thread_time = new Thread() {

        @Override
        public void run() {
            try {
                while (!isInterrupted()) {
                    Thread.sleep(1000);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            // update TextView here!
                            long new_time = (long) Math.floor(System.currentTimeMillis() / 1000);
                            long seconds = (new_time - resume_time) + diff_time;
                            int new_sec = (int) seconds;
                            int new_min = (int) Math.floor(new_sec / 60);
                            int new_hour = (int) Math.floor(new_min / 60);
                            while (new_sec >= 60) {
                                new_sec = (int) Math.floor(new_sec / 60);
                            }
                            while (new_min >= 60) {
                                new_min = (int) Math.floor(new_min / 60);
                            }
                            while (new_hour >= 24) {
                                new_hour = (int) Math.floor(new_hour / 24);
                            }
                            int show_seconds = new_sec;
                            int show_minutes = new_min;
                            int show_hour = new_hour;
                            round_time = (int) seconds;
                            String show_text = "";
                            if (show_seconds >= 0 && show_minutes >= 0 && show_hour <= 0) {
                                if (show_seconds <= 9) {
                                    show_text = show_minutes + ":" + "0" + show_seconds;
                                } else if (show_seconds >= 10) {
                                    show_text = show_minutes + ":" + show_seconds;
                                }
                            } else if (show_hour > 0) {
                                show_text = show_hour + ":" + show_minutes + ":" + seconds;
                            }
                            tv_time_in_game.setText(getResources().getString(R.string.time_round) + ":" + "\n" + "\t" + "\t" + "\t" + show_text);
                        }
                    });
                }
            } catch (InterruptedException e) {
            }
        }
    };

    AdView adView;
    AdRequest adRequest;

    //extras
    int[] infoExtras;
    boolean tg_top = false, tg_start = false, tg_center = false, tg_end = false, tg_bottom = false;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        back = false;
        setContentView(R.layout.activity_game);
        //Full screen mode
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        layout = (TableLayout) findViewById(R.id.tl_game);
        //get size field
        Intent intent = getIntent();
        infoExtras = intent.getExtras().getIntArray("info");
        size = infoExtras[0];
        if (infoExtras[1] == 1)
            tg_top = true;
        if (infoExtras[2] == 1)
            tg_start = true;
        if (infoExtras[3] == 1)
            tg_center = true;
        if (infoExtras[4] == 1)
            tg_end = true;
        if (infoExtras[5] == 1)
            tg_bottom = true;
        Log.d(LOG_TAG, "onCreate");
        //shared
        tv_tap_in_game = (TextView) findViewById(R.id.tv_tap_in_game);
        tv_time_in_game = (TextView) findViewById(R.id.tv_time_in_game);
        tv_tap_in_game.setText(getResources().getString(R.string.count_tap) + ":" + "\n" + "\t" + "\t" + "\t" + count);
        tv_time_in_game.setText(getResources().getString(R.string.time_round) + ":" + "\n" + "\t" + "\t" + "\t" + "0:00");
        cs = new CalculateStatistics();
        //build
        build_field(size, reset, test_mode);
        //time
        thread_time.start();
        //ads
        adView = new AdView(this);
        adView = (AdView) findViewById(R.id.ad_banner_game);
        adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        adView.setAdListener(new AdListener() {
            @Override
            public void onAdOpened() {
                // Save app state before going to the ad overlay.
                onPause();
            }
        });
    }

    public void play_press() {
        SharedPreferences sharedPreferences;
        sharedPreferences = getSharedPreferences("settings", MODE_PRIVATE);
        if (sharedPreferences.getBoolean("sound", true)) {
            stopService(new Intent(this, AudioPlay.class));
            startService(new Intent(this, AudioPlay.class));
        }
    }

    protected void onDestroy() {
        super.onDestroy();
        thread_time.interrupt();
        back = false;
        Log.d(LOG_TAG, "onDestroy");
    }

    protected void onResume() {
        super.onResume();
        View v = new View(getApplicationContext());
        onClick_reset(v);
        reset = false;
        //get time
        resume_time = (long) Math.floor(System.currentTimeMillis() / 1000);
        //get string
        get_string();
        //get shared
        //get_shared();
        //alerts
        alert_dialog_reset();
        Log.d(LOG_TAG, "onResume ");
    }

    protected void onPause() {
        super.onPause();
        stopService(new Intent(this, AudioPlay.class));
        thread_time.interrupt();
        pause_time = (long) Math.floor(System.currentTimeMillis() / 1000);
        diff_time = round_time;
        //round_time = round_time + (int) diff_time;
        Log.d(LOG_TAG, "onPause");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(LOG_TAG, "onRestart");
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (!Arrays.deepEquals(mass, mass_empty)) {
            try {
                mass[0] = savedInstanceState.getIntArray("mass0");
                mass[1] = savedInstanceState.getIntArray("mass1");
                mass[2] = savedInstanceState.getIntArray("mass2");
            } catch (ArrayIndexOutOfBoundsException exception) {
                //noting to do
            }
            try {
                mass[3] = savedInstanceState.getIntArray("mass3");
                mass[4] = savedInstanceState.getIntArray("mass4");
            } catch (ArrayIndexOutOfBoundsException exception) {
                //noting to do
            }
            try {
                mass[5] = savedInstanceState.getIntArray("mass5");
            } catch (ArrayIndexOutOfBoundsException exception) {
                //noting to do
            }
            try {
                mass[6] = savedInstanceState.getIntArray("mass6");
            } catch (ArrayIndexOutOfBoundsException exception) {
                //noting to do
            }
            try {
                mass[7] = savedInstanceState.getIntArray("mass7");
            } catch (ArrayIndexOutOfBoundsException exception) {
                //noting to do
            }
            try {
                mass[8] = savedInstanceState.getIntArray("mass8");
            } catch (ArrayIndexOutOfBoundsException exception) {
                //noting to do
            }
            try {
                count = savedInstanceState.getInt("count");
                round_time = savedInstanceState.getInt("round_time");
            } catch (Exception e) {
                //noting to do
            }
            try {
                tg_top = savedInstanceState.getBoolean("tg_top");
                tg_top = savedInstanceState.getBoolean("tg_start");
                tg_top = savedInstanceState.getBoolean("tg_center");
                tg_top = savedInstanceState.getBoolean("tg_end");
                tg_top = savedInstanceState.getBoolean("tg_bottom");
            } catch (Exception e) {
                //nothing, bithes
            }
        }

        Log.d(LOG_TAG, "onRestoreInstanceState");
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (!Arrays.deepEquals(mass, mass_empty)) {
            try {
                outState.putIntArray("mass0", mass[0]);
                outState.putIntArray("mass1", mass[1]);
                outState.putIntArray("mass2", mass[2]);
            } catch (ArrayIndexOutOfBoundsException exception) {
                //noting to do
            }
            try {
                outState.putIntArray("mass3", mass[3]);
                outState.putIntArray("mass4", mass[4]);
            } catch (ArrayIndexOutOfBoundsException exception) {
                //noting to do
            }
            try {
                outState.putIntArray("mass5", mass[5]);
            } catch (ArrayIndexOutOfBoundsException exception) {
                //noting to do
            }
            try {
                outState.putIntArray("mass6", mass[6]);
            } catch (ArrayIndexOutOfBoundsException exception) {
                //noting to do
            }
            try {
                outState.putIntArray("mass7", mass[7]);
            } catch (ArrayIndexOutOfBoundsException exception) {
                //noting to do
            }
            try {
                outState.putIntArray("mass8", mass[8]);
            } catch (ArrayIndexOutOfBoundsException exception) {
                //noting to do
            }
            outState.putInt("count", count);
            outState.putInt("round_time", round_time);
            //state of switcher
            outState.putBoolean("tg_top", tg_top);
            outState.putBoolean("tg_start", tg_start);
            outState.putBoolean("tg_center", tg_center);
            outState.putBoolean("tg_end", tg_end);
            outState.putBoolean("tg_bottom", tg_bottom);
        }
        Log.d(LOG_TAG, "onSaveInstanceState");
    }

    protected void onStart() {
        super.onStart();
        Log.d(LOG_TAG, "onStart");
    }

    protected void onStop() {
        super.onStop();
        Log.d(LOG_TAG, "onStop");
    }

    public void onBackPressed() {
        back = true;
        finish();
    }


    //onclick custom
    public class Listener implements View.OnClickListener {
        int x = 0;
        int y = 0;
        int level = 0;
        int[][] massive;
        public TableLayout layout;

        Listener(int i, int j, int size, int[][] mass) {
            this.x = i;
            this.y = j;
            this.level = size;
            this.massive = mass;
        }


        @Override
        public void onClick(View v) {
            /*This is shit so wrong, what I had pain in my ass. Don't touch this in future(!)*/
            if (tg_top)
                left_x();
            if (tg_start)
                xy();
            if (tg_center)
                top_y();
            if (tg_end)
                bottom_y();
            if (tg_bottom)
                right_x();
            play_press();
            cacl_value_and_strings();
            //generate alert
            alert_dialog_success();
            checker();
            tv_tap_in_game.setText(getResources().getString(R.string.count_tap) + ":" + "\n" + "\t" + "\t" + "\t" + String.valueOf(count));

        }

        //JUST DO IT
        void xy() {
            try {
                //x left
                if (massive[x][y] == 0) {
                    massive[x][y] = 1;
                    setBackgroundOn(size, x, y);
                } else if (massive[x][y] == 1) {
                    massive[x][y] = 0;
                    setBackgroundOff(size, x, y);
                }
            } catch (ArrayIndexOutOfBoundsException exception) {
                //noting to do
            }
            count++;
        }

        void left_x() {
            try {
                //x left
                if (massive[x - 1][y] == 0) {
                    massive[x - 1][y] = 1;
                    setBackgroundOn(size, x - 1, y);
                } else if (massive[x - 1][y] == 1) {
                    massive[x - 1][y] = 0;
                    setBackgroundOff(size, x - 1, y);
                }
            } catch (ArrayIndexOutOfBoundsException exception) {
                //noting to do
            }
        }

        void right_x() {
            try {
                //x right
                if (massive[x + 1][y] == 0) {
                    massive[x + 1][y] = 1;
                    setBackgroundOn(size, x + 1, y);
                } else if (massive[x + 1][y] == 1) {
                    massive[x + 1][y] = 0;
                    setBackgroundOff(size, x + 1, y);
                }
            } catch (ArrayIndexOutOfBoundsException exception) {
                //noting to do
            }
        }

        void top_y() {
            try {
                //y top
                if (massive[x][y - 1] == 0) {
                    massive[x][y - 1] = 1;
                    setBackgroundOn(size, x, y - 1);
                } else if (massive[x][y - 1] == 1) {
                    massive[x][y - 1] = 0;
                    setBackgroundOff(size, x, y - 1);
                }
            } catch (ArrayIndexOutOfBoundsException exception) {
                //noting to do
            }
        }

        void bottom_y() {
            try {
                //y bottom
                if (massive[x][y + 1] == 0) {
                    massive[x][y + 1] = 1;
                    setBackgroundOn(size, x, y + 1);
                } else if (massive[x][y + 1] == 1) {
                    massive[x][y + 1] = 0;
                    setBackgroundOff(size, x, y + 1);
                }
            } catch (ArrayIndexOutOfBoundsException exception) {
                //noting to do
            }
        }
    }
}